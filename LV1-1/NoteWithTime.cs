﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Runtime.CompilerServices;
using System.Text;

namespace LV1_1
{
    class NoteWithTime : note
    {
        DateTime time;
        public NoteWithTime()
        {

        }
        public NoteWithTime(string contentText, string author, int importanceLvl)
        : base(contentText, author, importanceLvl)
        {
            time = DateTime.Now;
        }
        public NoteWithTime(string contentText, string author, int importanceLvl, int day, int month, int year)
        : base(contentText, author, importanceLvl)
        {
            time = new DateTime(year,month,day);
        }
        public DateTime getTime() { return time; }
        public void setTime(int year, int month, int day)
        {
            time = new DateTime(year, month, day);
        }

        public override string ToString()
        {
            return base.ToString() + "\n Vrijeme: " + time;
        }
        
    }
}
