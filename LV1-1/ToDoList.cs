﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace LV1_1
{
    class ToDoList : NoteWithTime
    {
        List<note> notes=new List<note>();
        public void insertNoteWithTime(string contentText, string author, int importanceLvl, DateTime date)
        {
            NoteWithTime note = new NoteWithTime(contentText, author, importanceLvl, date.Day, date.Month, date.Year);
            notes.Add(note);
        }
        public void removeNotes(int ordinalNumber)
        {
            notes.RemoveAt(ordinalNumber - 1);
        }
        public void printAllNotes(ToDoList list)
        {
            int i = 1;
            foreach (note note in list.GetNotes())
            {
                Console.WriteLine("\nRedni broj: " + i, i++);
                Console.WriteLine(note);
            }
        }
        public void printConsoleMenu()
        {
            Console.WriteLine("\nIZBORNIK \nUnesite broj za zeljenu akciju: " +
                    "\n 1. Unesi zabiljesku " +
                    "\n 2. Ispisi sve zabiljeske " +
                    "\n 3. Izbrisi zabiljesku " +
                    "\n 4. Unesi zabilješku sa specifičnim datumom" +
                    "\n 777. Izlaz \n");
        }
        public void insertNote(ToDoList list, int importanceLvl, string contentText, string author)
        {
            note note = new note(contentText, author, importanceLvl);
            notes.Add(note);
        } 
        public DateTime consoleInputDate()
        {
            int day = 0, month = 0, year = 0;
            try { 
                    do
                    {
                        Console.WriteLine("Dan (brojcano): ");
                        day = Convert.ToInt32(Console.ReadLine());
                    } while (day > 31 || day < 1);
                    do
                    {
                        Console.WriteLine("Mjesec (brojcano): ");
                        month = Convert.ToInt32(Console.ReadLine());
                    } while (month > 12 || month < 1);
                    do
                    {
                        Console.WriteLine("Godina (brojcano): ");
                        year = Convert.ToInt32(Console.ReadLine());
                    } while (year > 2100 || year < 1900);
                }
            catch (Exception error)
                {
                    Console.WriteLine("Dan, mjesec, i godina mora biti broj!!!");
                }

            DateTime date = new DateTime(year, month, day);
            return date;
        }
        public int consoleInputImportanceLvl()
        {
            int importanceLvl = 0;
            try
            {
                do
                {
                    Console.WriteLine("Prioritet (1-5): ");
                    importanceLvl = Convert.ToInt32(Console.ReadLine());
                } while (importanceLvl > 5 || importanceLvl < 1);
            }
            catch (Exception error) { Console.WriteLine("Prioritet mora biti broj 1-5 !!!"); }
            return importanceLvl;
        }
        public List<note> GetNotes(){ return notes; }

    
        static void Main(string[] args)
        {
            ToDoList list = new ToDoList();
            int menuInput=0;
            while (true)
            {
                list.printConsoleMenu();
                try { menuInput = Convert.ToInt32(Console.ReadLine()); }
                catch(Exception error){ Console.WriteLine("Unesite broj!!!"); }

                if (menuInput == 1)
                {
                    Console.WriteLine("Zabiljeska: ");
                    String contentText = Console.ReadLine();
                    Console.WriteLine("Autor: ");
                    String author = Console.ReadLine();
                    list.insertNote(list, list.consoleInputImportanceLvl(), contentText, author); 
                }
                else if (menuInput == 2) { list.printAllNotes(list); }
                else if (menuInput == 3)
                {
                    Console.WriteLine("Unesite redni broj zabiljeske koju zelite ukloniti: ");
                    try 
                    {
                        int ordinalNumber = Convert.ToInt32(Console.ReadLine());
                        list.removeNotes(ordinalNumber); 
                    }
                    catch (Exception error) { Console.WriteLine("Neispravan unos!"); }
                }
                else if (menuInput == 4)
                {
                    Console.WriteLine("Zabiljeska: ");
                    String contentText = Console.ReadLine();
                    Console.WriteLine("Autor: ");
                    String author = Console.ReadLine();
                    list.insertNoteWithTime(contentText, author, list.consoleInputImportanceLvl(), list.consoleInputDate());
                }
                else if (menuInput == 777) { break;}
                else{ Console.WriteLine("Pogresan unos!!");}
            }
    }
    }
}
