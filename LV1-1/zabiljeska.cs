﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV1_1
{
    class note
    {
        public int importanceLvl;
        public string author;
        public string contentText;
        public int ImportanceLvl
        {
            get { return importanceLvl; }
            set { importanceLvl = value; }
        }
        public string Author
        {
            get { return author; }
            private set { author = value; }
        }
        public string ContentText
        {
            get { return contentText; }
            set { contentText = value; }
        }

        public note()
        {
            contentText = "Ako imaš mladu ženu i staro vino, uvijek imaš puno gostiju.";
            author = "Denis Ivanovic";
            importanceLvl = 2;
        }
        note(int importanceLvl)
        {
            contentText = "Ko hoće daleko stići, mora rano ustati.";
            author = "Denis Ivanovic";
            this.importanceLvl = importanceLvl;
        }
        private note(string contentText)
        {
            this.contentText = contentText;
            author = "rootUser";
            importanceLvl = 0;
        }

        public note(string contentText, string author, int importanceLvl)
        {
            this.contentText = contentText;
            this.author = author;

            try {
                if (importanceLvl > 0 && importanceLvl < 6){ this.importanceLvl = importanceLvl; }
                else {
                    ArgumentException error = new ArgumentException("Importance level must be from 1 to 5 !!", "importanceLvl");
                    throw error;
                    }
            }
            catch (Exception error){
                Console.WriteLine("Prioritet mora biti broj od 1 do 5 !!");
            }  
        }


        public int getImportanceLvl() { return importanceLvl; }
        public string getAuthor() { return author; }

        public void setImportanceLvl(int lvl)
        {
            try { if (lvl > 0 && lvl < 6) { importanceLvl = lvl; }
                else{
                    ArgumentException error = new ArgumentException("Importance level must be from 1 to 5 !!", "importanceLvl");
                    throw error;
                }
            }

            catch (Exception error)
            {
                Console.WriteLine(error);
            }
        }

        public override string ToString()
        {
            return " Bilješka: " + this.contentText 
                + "\n Autor: " + this.author 
                + "\n Prioritet: " + this.importanceLvl;
        }
     
    }


}
